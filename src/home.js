import React from "react";
import { View, Button } from "react-native";
import { useNavigation } from "@react-navigation/native";

const HomeScreen = () => {
  const navigation = useNavigation();

  const handleCount = () => {
    navigation.navigate("CountMovement");
  };
  const handleImage = () => {
    navigation.navigate("ImageMovement");
  };

  return (
    <View>
      <Button title="Count" onPress={handleCount} />
      <Button title="Image" onPress={handleImage} />
    </View>
  );
};
export default HomeScreen;

