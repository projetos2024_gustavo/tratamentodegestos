import React from 'react';
import countMoviment from './src/countMoviment';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import imageMovement from './src/imageMovimento';
import HomeScreen from './src/home';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='HomeScreen'>
        <Stack.Screen name="CountMoviment" component={countMoviment}/>
        <Stack.Screen name="ImageMovement" component={imageMovement}/>
        <Stack.Screen name="HomeScreen" component={HomeScreen}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

